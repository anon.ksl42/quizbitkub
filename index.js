const axios = require("axios");
const config = require("./config");

const fetchData = async () => {
  const uriFromquery = `${config.uri}?module=${config.env.module}&action=${config.env.action}&address=${config.env.address}&startblock=${config.env.startblock}&endblock=${config.env.endblock}&sort=${config.env.sort}&apikey=${config.env.apikey}`;
  let res = await axios.get(uriFromquery);
  return res.data;
};

const result = fetchData().then((res) => {
  const findData = res.result.filter(
    (res) => res.from === "0xeca19b1a87442b0c25801b809bf567a6ca87b1da"
  );
  for (let i in findData) {
    let data = {
      hash: findData[i].hash,
      from: findData[i].from,
      to: findData[i].to,
      Amount: parseFloat(findData[i].value),
    };
    console.log("Output1", data);
    // console.log("Output2",data);
  }
});
